core = 7.x
api = 2

; uw_orientation_schedule
projects[uw_orientation_schedule][type] = "module"
projects[uw_orientation_schedule][download][type] = "git"
projects[uw_orientation_schedule][download][url] = "https://git.uwaterloo.ca/wcms/uw_orientation_schedule.git"
projects[uw_orientation_schedule][download][tag] = "7.x-1.22"
projects[uw_orientation_schedule][subdir] = ""
